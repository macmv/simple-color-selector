mod print;

use termion::{
  event::*,
  input::TermRead,
  input::MouseTerminal,
  raw::IntoRawMode,
};
use std::io::{
  self,
};

fn main() {
  let stdin = io::stdin();
  let mut stdout = MouseTerminal::from(io::stdout().into_raw_mode().unwrap());
  let mut screen = print::screen::new(&mut stdout);
  screen.clear();

  for c in stdin.events() {
    let evt = c.unwrap();
    match evt {
      Event::Key(Key::Char('q')) => break,
      Event::Mouse(e) => {
        match e {
          MouseEvent::Press(_, x, y) |
            MouseEvent::Hold(x, y) => {
              screen.select(x, y);
            }
          MouseEvent::Release(x, y) => {
            // write!(stdout, "{}R", cursor::Goto(x, y)).unwrap();
          }
        }
      }
      _ => {}
    }
    screen.flush();
  }

  screen.finish();
}
