use std::io::{
  self,
};

#[derive(Copy, Clone)]
pub struct Color {
  pub r: u8,
  pub g: u8,
  pub b: u8,
}

pub fn from_rgb(r: u8, g: u8, b: u8) -> Color {
  return Color{r: r, g: g, b: b}
}

pub fn from_hsv(h: u8, s: u8, v: u8) -> Color {
  // chroma
  let c = (v as u32 * s as u32 / 255) as u8;
  // 42.6 is 256 / 6
  let x = ((1.0 - (h as f64 / 42.6 % 2.0 - 1.0).abs()) * c as f64) as u8;
  let m = v - c;
  if h < 42 {
    return from_rgb(m + c, m + x, m + 0);
  } else if h >= 42 && h < 85 {
    return from_rgb(m + x, m + c, m + 0);
  } else if h >= 85 && h < 127 {
    return from_rgb(m + 0, m + c, m + x);
  } else if h >= 127 && h < 170 {
    return from_rgb(m + 0, m + x, m + c);
  } else if h >= 170 && h < 213 {
    return from_rgb(m + x, m + 0, m + c);
  } else {
    return from_rgb(m + c, m + 0, m + x);
  }
}

impl Color {
  pub fn render(&self, text: &str, out: &mut dyn io::Write) {
    write!(out,
           "\x1b[38;2;{};{};{}m{}",
           self.r,
           self.g,
           self.b,
           text).unwrap();
  }
  pub fn print(&self, text: &str) {
    print!("\x1b[38;2;{};{};{}m{}", self.r, self.g, self.b, text)
  }
}
