
use base64;
use crate::print::color;
use std::io::{
  self,
};

pub struct Screen<'a> {
  out: &'a mut dyn io::Write,
  selected_x: u16,
  selected_y: u16,
  hue: u8,
  saturation: u8,
  value: u8,
  copied: bool,
}

pub fn new<'a>(out: &'a mut dyn io::Write) -> Screen {
  return Screen{
    out: out,
    selected_x: 0,
    selected_y: 0,
    hue: 0,
    saturation: 0,
    value: 255,
    copied: true,
  };
}

impl Screen<'_> {
  pub fn clear(&mut self) {
    write!(self.out,
           "{}{}{}",
           termion::clear::All,
           termion::cursor::Hide,
           termion::cursor::Goto(1, 1))
      .unwrap();
    self.render_hue();
    self.render_value();
    self.flush();
  }
  pub fn flush(&mut self) {
    self.out.flush().unwrap();
  }
  pub fn finish(&mut self) {
    write!(self.out,
           "{}",
           termion::cursor::Show)
      .unwrap();
  }
  pub fn select(&mut self, x: u16, y: u16) {
    if self.copied {
      // This clears the 'Copied!' message, if there was one displayed
      for y in 4..8 {
        write!(self.out,
               "{}       ",
               termion::cursor::Goto(121, y))
          .unwrap();
      }
      // Printing these spaces is slow, so we only want to do it when needed.
      self.copied = false;
    }
    if y > 32 {
      return
    }
    let pos_x = (x - 1) / 2;
    let pos_y = (y - 1) / 2;
    if x <= 64 {
      // Clicked in hue box
      self.update_selection(pos_x, pos_y);
      self.render_value();
    } else if x > 68 && x <= 72 {
      // Clicked on hue slider
      let mut new_pos = self.selected_x as i16 - 8 + pos_y as i16;
      if new_pos >= 32 {
        new_pos -= 32
      }
      if new_pos < 0 {
        new_pos += 32
      }
      self.update_selection(new_pos as u16, self.selected_y);
      self.render_value();
    } else if x > 80 && x <= 84 {
      // Clicked on saturation slider
      self.update_selection(self.selected_x, pos_y);
      self.render_value();
    } else if x > 92 && x <= 96 {
      // Clicked on value slider
      self.value = (pos_y * 16 + pos_y) as u8;
      self.render_hue();
      self.render_value();
    } else if x > 105 && x < 120 {
      // Clicked on one of the color values
      let data;
      let color = color::from_hsv(self.hue, self.saturation, self.value);
      if y == 4 {
        // #fff
        data = format!("#{:x}{:x}{:x}",
                       color.r / 16,
                       color.g / 16,
                       color.b / 16);
      } else if y == 5 {
        // #ffffff
        data = format!("#{:02x}{:02x}{:02x}",
                       color.r,
                       color.g,
                       color.b);
      } else if y == 6 {
        // 255 255 255
        data = format!("{:>3} {:>3} {:>3}",
                       color.r,
                       color.g,
                       color.b);
      } else if y == 7 {
        // 1.00 1.00 1.00
        data = format!("{:.2} {:.2} {:.2}",
                       color.r as f64 / 255.0,
                       color.g as f64 / 255.0,
                       color.b as f64 / 255.0);
      } else {
        return
      }
      // This copies whatever is in data to your clipboard
      write!(self.out,
             "\x1b]52;;{}\x1b\\",
             base64::encode(data))
        .unwrap();
      write!(self.out,
             "{}Copied!",
             termion::cursor::Goto(121, y))
        .unwrap();
      // This is set so that the 'Copied!' text is cleared next time you click
      self.copied = true;
      return
    }
  }
  fn update_selection(&mut self, x: u16, y: u16) {
    let color = color::from_hsv(self.hue, self.saturation, self.value);
    self.render_block(self.selected_x, self.selected_y, color);

    self.selected_x = x;
    self.selected_y = y;
    self.hue = (x * 8) as u8;
    self.saturation = (y * 16 + y) as u8;
    let color = color::from_hsv(self.hue, self.saturation, self.value);
    self.render_selected_block(self.selected_x, self.selected_y, color);
  }
  fn render_hue(&mut self) {
    for y in 0..32 {
      write!(self.out,
             "{}",
             termion::cursor::Goto(1, (y + 1).into()))
        .unwrap();
      for x in 0..32 {
        let h = x * 8;
        let s = (y / 2) * 16 + (y / 2);
        let color = color::from_hsv(h, s, self.value);
        if s == self.saturation && h == self.hue {
          if y % 2 == 0 {
            color.render("▗▖", self.out);
          } else {
            color.render("▝▘", self.out);
          }
        } else {
          color.render("██", self.out);
        }
      }
    }
  }
  fn render_value(&mut self) {
    let start_x = 34;
    for y in 0..16 {
      // We add y so that the color range is 0-255, not 0-(16*15)
      let val = (y * 16 + y) as u8;
      let hue = (self.hue as i16 - (4 * 16 + 4) + val as i16 / 2) as u8;
      let color = color::from_hsv(hue, self.saturation, self.value);
      if self.hue == hue {
        self.render_selected_rgb(start_x, y, color);
      } else {
        self.render_rgb(start_x, y, color);
      }
      let color = color::from_hsv(self.hue, val, self.value);
      if self.saturation == val {
        self.render_selected_rgb(start_x + 6, y, color);
      } else {
        self.render_rgb(start_x + 6, y, color);
      }
      let color = color::from_hsv(self.hue, self.saturation, val);
      if self.value == val {
        self.render_selected_rgb(start_x + 12, y, color);
      } else {
        self.render_rgb(start_x + 12, y, color);
      }
    }
    let color = color::from_hsv(self.hue, self.saturation, self.value);
    self.render_wide_block(start_x + 18, 0, color);
    write!(self.out,
           "{}#{:x}{:x}{:x}",
           termion::cursor::Goto((start_x + 18) * 2 + 2, 4),
           color.r / 16,
           color.g / 16,
           color.b / 16)
      .unwrap();
    write!(self.out,
           "{}#{:02x}{:02x}{:02x}",
           termion::cursor::Goto((start_x + 18) * 2 + 2, 5),
           color.r,
           color.g,
           color.b)
      .unwrap();
    write!(self.out,
           "{}{:>3} {:>3} {:>3}",
           termion::cursor::Goto((start_x + 18) * 2 + 2, 6),
           color.r,
           color.g,
           color.b)
      .unwrap();
    write!(self.out,
           "{}{:.2} {:.2} {:.2}",
           termion::cursor::Goto((start_x + 18) * 2 + 2, 7),
           color.r as f64 / 255.0,
           color.g as f64 / 255.0,
           color.b as f64 / 255.0)
      .unwrap();
  }
  fn render_selected_rgb(&mut self, x: u16, y: u16, color: color::Color) {
    self.render_selected_wide_block(x, y, color);
    write!(self.out,
           "{}#{:x}{:x}{:x}",
           termion::cursor::Goto((x * 2) + 7, (y * 2) + 1),
           color.r / 16,
           color.g / 16,
           color.b / 16)
      .unwrap();
  }
  fn render_rgb(&mut self, x: u16, y: u16, color: color::Color) {
    self.render_wide_block(x, y, color);
    write!(self.out,
           "{}#{:x}{:x}{:x}",
           termion::cursor::Goto((x * 2) + 7, (y * 2) + 1),
           color.r / 16,
           color.g / 16,
           color.b / 16)
      .unwrap();
  }
  fn render_selected_block(&mut self, x: u16, y: u16, color: color::Color) {
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 1).into()))
      .unwrap();
    color.render("▗▖", self.out);
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 2).into()))
      .unwrap();
    color.render("▝▘", self.out);
  }
  fn render_selected_wide_block(&mut self, x: u16, y: u16, color: color::Color) {
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 1).into()))
      .unwrap();
    color.render(" ▄▄ ", self.out);
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 2).into()))
      .unwrap();
    color.render(" ▀▀ ", self.out);
  }
  fn render_block(&mut self, x: u16, y: u16, color: color::Color) {
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 1).into()))
      .unwrap();
    color.render("██", self.out);
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 2).into()))
      .unwrap();
    color.render("██", self.out);
  }
  fn render_wide_block(&mut self, x: u16, y: u16, color: color::Color) {
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 1).into()))
      .unwrap();
    color.render("████", self.out);
    write!(self.out,
           "{}",
           termion::cursor::Goto((x * 2 + 1).into(), (y * 2 + 2).into()))
      .unwrap();
    color.render("████", self.out);
  }
}
